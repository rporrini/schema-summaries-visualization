package it.unimib.disco.summarization.tests;

import it.unimib.disco.summarization.utility.InputFile;
import it.unimib.disco.summarization.utility.Processing;

import java.util.ArrayList;
import java.util.List;

public class ProcessingInspector implements Processing{

	List<InputFile> processedFile = new ArrayList<InputFile>();
	
	@Override
	public void process(InputFile file) throws Exception {
		processedFile.add(file);
	}

	@Override
	public void endProcessing() throws Exception {}
	
	public int countProcessed(){
		return processedFile.size();
	}
}