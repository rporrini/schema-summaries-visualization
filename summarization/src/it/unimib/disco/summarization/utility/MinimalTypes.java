package it.unimib.disco.summarization.utility;

import java.util.List;

public interface MinimalTypes {

	public List<String> of(String entity);

}