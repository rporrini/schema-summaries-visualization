#!/bin/bash

set -e

ssh -i ~/schema-summaries/deployment/deploy_rsa $1 $2
